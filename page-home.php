<?php
/*
 Template Name: Home Page
*/
?>

<?php get_header(); ?>
<section role="content" id="about" class="about clearfix">
  <div class="container">
    <div class="about__introText clearfix">
      <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/about_Jafanda-a.svg" class="about__aIcon">
      <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
        <span>Jafända</span> is in the business of creating advanced control of everyday air environments, weather that be in the home or commercial space. Our mission is to become the air specialist of choice for all consumer and commercial  uses.
      <?php endif; ?>
      <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
        <span>Jafända</span> bietet eine hoch entwickelte Lösung für Ihre gesunde Luftatmosphäre, sowohl zuhause als auch für Gewerberäume. Unser Anspruch ist, beste Produkte nach höchstem industriellen Standard kommerziellen und privaten Kunden für eine gesunde Umgebung und ein Leben in bestmöglicher Gesundheit anzubieten.
      <?php endif; ?>
    </div>
    <div class="about__hamburgText">
      <img src="<?php echo get_template_directory_uri(); ?>/library/images/about_hamburg.jpg" class="about__hamburgImg">
      <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
        <p class="text--bold text--marginBottom text--intro">
          <span class="text--extraBold">Jafända</span> founded in Germany's second largest city, the manufacturing center of Hamburg. Leveraging German technologies and engineering, we supply high quality commercial & consumer air purification solutions to Germany and overseas market.
        </p>
        <p class="text--noMargin">
          Jafända is designed for families who appreciate that every breath counts. Our advanced air control solutions make it possible to enjoy clean, healthy air both at home and at work, ensuring the highest quality air through out your day. With our driven team of engineers we leverage proven German technologies to deliver <span class="text--bold">high performance, sustainability and energy efficiency</span>.
        </p>
      <?php endif; ?>
      <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
        <p class="text--bold text--marginBottom text--intro">
          <span class="text--extraBold">Jafända</span> Lufttechnologie GmbH hat ihren Firmensitz in Hamburg, die zweitgrößte Stadt in Deutschland. Mit der Technologie aus Deutschland werden Lösungskonzepte mit hohem Standard für kommerzielle und private Kunden dargeboten.
        </p>
        <p class="text--noMargin">
          Jafända Produkte sind konstruiert für Menschen mit hoher Anforderung an die Luftqualität. Unsere Produkte versorgen mit gesunder und frischer Luft im Innenraum. Innovativ, zuverlässig, aufgeschlossen zu sein, ist die Arbeitsphilosophie des Ingenieur Teams. Das Team hält fest an den Optimierungsprinzip, um das Beste für die Kunden zu ermöglichen.
        </p>
      <?php endif; ?>
    </div>
  </div>
</section>
<section role="content" class="products clearfix">
  <nav class="nav productNav">
    <ul class="productNav__tabs">
      <li class="productNav__tabsItem is-active">AFU-01</li>
      <li class="productNav__tabsItem">AFU-11</li>
      <li class="productNav__tabsItem">JD-500</li>
    </ul>
  </nav>
  <!-- Loop through products -->
  <div class="slider clearfix">
    <?php
      $args = array('post_type' => 'product', 'posts_per_page' => 3, 'orderby' => 'title', 'order' => 'ASC');
      $products = get_posts($args);

      foreach($products as $post) : setup_postdata( $post );
        $imagesTop = get_field("product_images_top");
        $imagesBottom = get_field("product_images_bottom");

        // Include images if they are available
        $topImage = count($imagesTop) > 0 ? $imagesTop[0]["sizes"]["bones-thumb-758"] : "";
        $bottomImage = count($imagesBottom) > 0 ? $imagesBottom[0]["sizes"]["bones-thumb-758"] : "";
      ?>
      <section role="product" class="product slide clearfix" id="products">
        <div class="product__productImages clearfix">
          <div class="product__productImagesShadow"></div>
          <div class="product__productImagesTop js-replace-bg" style="background-image: url(<?= $topImage; ?>);">
            <div class="product__productImagesNav">
              <?php if(count($imagesTop) > 1) : ?>
                <?php foreach($imagesTop as $image) : ?>
                  <a href="<?= $image["sizes"]["bones-thumb-758"]; ?>" class="product__productImagesNavLink js-images-nav">
                    <img src="<?= $image["sizes"]["thumbnail"]; ?>" class="product__productImagesNavItem">
                  </a>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="product__productImagesBottom js-replace-bg" style="background-image: url(<?= $bottomImage; ?>);">
            <div class="product__productImagesNav">
              <?php if(count($imagesBottom) > 1) : ?>
                <?php foreach($imagesBottom as $image) : ?>
                  <a href="<?= $image["sizes"]["bones-thumb-758"]; ?>" class="product__productImagesNavLink js-images-nav">
                    <img src="<?= $image["sizes"]["thumbnail"]; ?>" class="product__productImagesNavItem">
                  </a>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="productDescription">
          <div class="product__titleLine"></div>
          <h3 class="heading--three">Products</h3>
          <div class="product__productTitle"><?php the_title(); ?></div>
          <div class="product__productDescription text--inverse">
            <p class="text--inverse text--intro text--bold text--marginBottom">
              <?= get_field("product_intro"); ?>
            </p>
            <p class="text--inverse text--marginBottom product__productDescriptionText">
              <?= get_the_content(); ?>
            </p>
          </div>
          <?php if(get_field("technical_specifications")): ?>
          <div class="product__productDownload">
            <h4 class="heading--four">Download</h4>
            <ul class="downloadList downloadList--product">
              <li><a href="<?php the_field("technical_specifications"); ?>" class="downloadList__item">
                <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
                  Technical Specs (table 1)
                <?php endif; ?>
                <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
                  Das Sortiment (Siehe die Anlage)
                <?php endif; ?>
              </a></li>
            </ul>
          </div>
          <?php endif; ?>
        </div>
      </section>
    <?php endforeach; wp_reset_postdata(); ?>
  </div>
</section>
<?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
<section role="content" class="services" id="services">
  <div class="container">
    <div class="services__titleWithBg">
      <h2 class="heading--two">SERVICES</h2>
    </div>
    <div class="largeList">
      <div class="largeList__number">1</div>
      <div class="largeList__titleText">
        <span class="text--extraBold">PART 1</span><br>
        Installation and maintenance
      </div>
      <div class="largeList__sub">
        Our agencies will provide you installation and maintenance services.
      </div>
      <div class="largeList__hr"></div>
      <div class="largeList__downloads">
        <h4 class="heading--four">Download</h4>
        <ul class="downloadList">
          <li><a href="<?php the_field("installation"); ?>" class="downloadList__item">Installation (table 2)</a></li>
          <li><a href="<?php the_field("maintenance"); ?>" class="downloadList__item">Maintenance Installation(table 2)</a></li>
        </ul>
      </div>
    </div>
    <div class="largeList">
      <div class="largeList__number">2</div>
      <div class="largeList__titleText">
        <span class="text--extraBold">PART 2</span><br>
        Data Download
      </div>
      <div class="largeList__hr"></div>
      <div class="largeList__downloads">
        <h4 class="heading--four">Download</h4>
        <ul class="downloadList">
          <li><a href="<?php the_field("afu_11_ins"); ?>" class="downloadList__item">Jafända Afu-11 Instruction Download</a></li>
          <li><a href="<?php the_field("afu_01_ins"); ?>" class="downloadList__item">Jafända Afu-01 Instruction Download</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
<section role="content" class="services" id="services">
  <div class="container">
    <div class="services__titleWithBg">
      <h2 class="heading--two">SERVICES</h2>
    </div>
    <div class="largeList">
      <div class="largeList__number">1</div>
      <div class="largeList__titleText">
        <span class="text--extraBold">PART 1</span><br>
        Installation und Service für Wartung und Pflege
      </div>
      <div class="largeList__sub">
        Unsere Vertriebspartner werden unsere Produkte für Sie installieren und Ihnen für diese Wartung und Pflege anbieten.
      </div>
      <div class="largeList__hr"></div>
      <div class="largeList__downloads">
        <h4 class="heading--four">Download</h4>
        <ul class="downloadList">
          <li><a href="<?php the_field("installation"); ?>" class="downloadList__item">Installation (Anlage 2)</a></li>
          <li><a href="<?php the_field("maintenance"); ?>" class="downloadList__item">Wartung und Pflege (Anlage 3) </a></li>
        </ul>
      </div>
    </div>
    <div class="largeList">
      <div class="largeList__number">2</div>
      <div class="largeList__titleText">
        <span class="text--extraBold">PART 2</span><br>
        Down Loading
      </div>
      <div class="largeList__hr"></div>
      <div class="largeList__downloads">
        <h4 class="heading--four">Download</h4>
        <ul class="downloadList">
          <li><a href="<?php the_field("afu_11_ins"); ?>" class="downloadList__item">Gebrauchsanweisung für Jafända Afu-11</a></li>
          <li><a href="<?php the_field("afu_01_ins"); ?>" class="downloadList__item">Gebrauchsanweisung für Jafända Afu-01</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<section role="content" class="faq" id="faqs">
  <div class="container">
    <div class="faq__header clearfix">
      <h2 class="heading--two faq__headerTitle">FAQ</h2>
      <div class="faq__headerHr"></div>
      <a href="mailto:ask@jafaenda.de" class="faq__askQuestion">ASK A QUESTION?</a>
    </div>
    <?php
      $counter = 0;
      $args = array('post_type' => 'faq', 'posts_per_page' => 6, 'order' => 'ASC');
      $faq_raw = get_posts($args);
      $faq_chunks = array_chunk($faq_raw, 2);
      foreach ( $faq_chunks as $faqs ):
    ?>
      <div class="faq__row clearfix">
        <?php foreach ( $faqs as $post ) :
          setup_postdata( $post );
          $counter++;
        ?>
          <div class="faq__item" id="<?php echo $counter; ?>">
            <div class="faq__number">
              <?php echo $counter; ?>
            </div>
            <div class="faq__question">
              <?php the_title(); ?>
            </div>
            <div class="faq__answer">
              <?php the_content(); ?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
