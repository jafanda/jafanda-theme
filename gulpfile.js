// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var browserSync = require('browser-sync').create();

// Lint Task
gulp.task('lint', function() {
  return gulp.src('js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
  return gulp.src('library/scss/*.scss')
    .pipe(plumber(function (error) {
        gutil.log(error.message);
        this.emit('end');
    }))
    .pipe(plumber())
    .pipe(concat('style.css'))
    .pipe(sass())
    .pipe(gulp.dest('.'))
    .pipe(notify({message: 'Styles task complete'}));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
  return gulp.src('js/*.js')
    .pipe(plumber(function (error) {
        gutil.log(error.message);
        this.emit('end');
    }))
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
    .pipe(notify({message: 'Scripts task complete'}));
});

// Watch Files For Changes
gulp.task('watch', function() {
  browserSync.init({
    files: '*.php',
    proxy: "jafanda.dev",
    open: "local",
    browser: "google chrome",
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch('library/**/*.scss', ['sass']);
  gulp.watch('js/*.js', ['scripts', 'lint']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
