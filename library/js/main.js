$(function(){
  $(window).scroll(function() {
      var y_scroll_pos = window.pageYOffset;
      var scroll_pos_test = 10;
      var nav = $('.js-navigation');

      if(y_scroll_pos > scroll_pos_test && navigator.userAgent.match(/iPad/i) === null) {
          if(!nav.hasClass('is-fixed')){
            nav.addClass('is-fixed');
          }
      }
      if(y_scroll_pos < scroll_pos_test) {
          if(nav.hasClass('is-fixed')){
            nav.removeClass('is-fixed');
          }
      }
  });

  /**
   * This part causes smooth scrolling using scrollto.js
   * We target all a tags inside the nav, and apply the scrollto.js to it.
   */
  $(".navMenu__menuList a").click(function(evn){
      evn.preventDefault();
      var ahref = $(this).attr('href');
      $('html, body').animate({
        scrollTop: $(ahref).offset().top - 100
      }, 1000);
  });

  /**
   * This part handles the highlighting functionality.
   * We use the scroll functionality again, some array creation and
   * manipulation, class adding and class removing, and conditional testing
   */
  var aChildren = $(".navMenu__menuList li").children(); // find the a children of the list items
  var aArray = []; // create the empty aArray

  for (var i=0; i < aChildren.length; i++) {

      var aChild = aChildren[i];
      var ahref = $(aChild).attr('href');

      aArray.push(ahref);
  } // this for loop fills the aArray with attribute href values

  $(window).scroll(function(){
      var windowPos = window.pageYOffset; // get the offset of the window from the top of page
      var windowHeight = $(window).height(); // get the height of the window
      var docHeight = $(document).height();

      for (var i=0; i < aArray.length; i++) {
          var theID = aArray[i];
          var div = $(theID).offset();

          var divPos = div.top - 100; // get the offset of the div from the top of page

          var divHeight = $(theID).height(); // get the height of the div in question
          if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
              $("a[href='" + theID + "']").parent().addClass("nav-active");
          } else {
              $("a[href='" + theID + "']").parent().removeClass("nav-active");
          }
      }

      if(windowPos + windowHeight == docHeight) {
          if (!$("nav li:last-child a").parent().hasClass("nav-active")) {
              $(".navMenu__menuList li:last-child").addClass("nav-active").siblings().removeClass("nav-active");
          }
      }
  });
});

$(".slider").simpleSlider({
  magneticSwipe: false,
  neverEnding: false,
  slideOnInterval: false,
  animateDuration: 500
});

var slider = $(".slider").data("simpleslider");

$(".productNav__tabsItem").click(function(){
  var $currentEl = $(this);
  slider.nextSlide($currentEl.index());
  $currentEl.addClass("is-active").siblings().removeClass("is-active");
});

$(".slider").on("beforeSliding", function(event){
  $(".productNav__tabsItem").eq(event.newSlide).addClass("is-active").siblings().removeClass("is-active");
});


$(".navMenu__mobileMenu").click(function(){
  var $currentEl = $(this);
  $currentEl.toggleClass("is-active");
  $(".navMenu__menuList").toggleClass("is-active");
});

// Add active class to nav
$(".product__productImagesNavLink:first-child").addClass("is-active");

$(".js-images-nav").click(function(e){
  e.preventDefault();
  var $currentEl = $(this);
  $currentEl.addClass("is-active").siblings().removeClass("is-active");
  $currentEl.closest(".js-replace-bg").css({
    "background-image" : "url(" + $currentEl.attr("href") + ")"
  });
  console.log($currentEl.attr("href"));
});

window.onerror = function(error) {
    alert(error);
};
