<footer role="footer" class="footer clearfix" id="contact">
  <div class="footer__jafandaLogoContainer">
    <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/footer_jafanda.svg" class="footer__jafandaLogo">
  </div>
  <div class="container">
    <div class="footer__left">
      <div class="footer__subscribe text--inverse">
      <div class="footer__titleHr"></div>
      <h3 class="heading--three">Contact</h3>
        <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
          Do you have a question about product or our company? Please use the contact form at the bottom:
        <?php endif; ?>
        <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
          Wollen Sie sich über unsere Produkte weiter informieren? Dann nehmen Sie Kontakt mit uns:
        <?php endif; ?>
        <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 5 ); } ?>
      </div>
    </div>
    <div class="footer__right">
      <h4 class="heading--four footer__smallHeading">ADDRESS</h4>
      <div class="footer__address">
        Bottgerstr.12<br>
        20148 Hamburg <br>
        Germany
      </div>
      <div class="footer__telephone">
        <h4 class="heading--four footer__smallHeading">TELEPHONE</h4>
        <span class="footer__smallHeadingSub">+49 040 4135 5991</span>
      </div>
      <div class="footer__mapView">
        <h4 class="heading--four footer__smallHeading">MAP VIEW</h4>
        <a href="#" class="footer__mapViewLink footer__smallHeadingSub">VIEW JAFÄNDA ON GOOGLE MAPS</a>
      </div>
    </div>
    <div class="footer__copyright">
        Copyright Jafända 2015 - All rights reserved
      </div>
  </div>
</footer>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
