<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/library/images/favicon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <link href='//fonts.googleapis.com/css?family=Rajdhani:300,400,500,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<header role="header" id="home" class="header">
      <nav class="nav navMenu js-navigation">
        <div class="languages">
          <ul class="languages__list">
            <li class="languages__item">
              <a href="<?php echo site_url(); ?>/en" class="languages__link <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>is-active<?php endif; ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/library/images/flags/GB.svg" class="languages__GB languages__img">
              </a>
            </li>
            <li class="languages__item">
              <a href="<?php echo site_url(); ?>/de" class="languages__link <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>is-active<?php endif; ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/library/images/flags/DE.svg" class="languages__DE languages__img">
              </a>
            </li>
          </ul>
        </div>

        <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/header_jafanda-logo.svg" class="navMenu__logo">
        <div class="navMenu__mobileMenu">
          <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/menu-icon.svg">
        </div>
        <?php if (get_bloginfo( 'language' ) == 'en-GB'): ?>
          <ul class="navMenu__menuList">
            <li class="navMenu__menuItem"><a href="#home" class="navMenu__menuItemLink">Home</a></li>
            <li class="navMenu__menuItem"><a href="#about" class="navMenu__menuItemLink">About</a></li>
            <li class="navMenu__menuItem"><a href="#products" class="navMenu__menuItemLink">Products</a></li>
            <li class="navMenu__menuItem"><a href="#services" class="navMenu__menuItemLink">Service</a></li>
            <li class="navMenu__menuItem"><a href="#faqs" class="navMenu__menuItemLink">Faq</a></li>
            <li class="navMenu__menuItem"><a href="#contact" class="navMenu__menuItemLink">Contact</a></li>
          </ul>
        <?php endif; ?>
        <?php if (get_bloginfo( 'language' ) == 'de-DE'): ?>
          <ul class="navMenu__menuList">
            <li class="navMenu__menuItem"><a href="#home" class="navMenu__menuItemLink">HAUPTSEITE</a></li>
            <li class="navMenu__menuItem"><a href="#about" class="navMenu__menuItemLink">ÜBER</a></li>
            <li class="navMenu__menuItem"><a href="#products" class="navMenu__menuItemLink">PRODUKT</a></li>
            <li class="navMenu__menuItem"><a href="#services" class="navMenu__menuItemLink">DIENSTLEISTUNG</a></li>
            <li class="navMenu__menuItem"><a href="#faqs" class="navMenu__menuItemLink">HÄUGIF GESTELLTE FRAGEN</a></li>
            <li class="navMenu__menuItem"><a href="#contact" class="navMenu__menuItemLink">KONTAKT</a></li>
          </ul>
        <?php endif; ?>
      </nav>
      <div class="header__greenBox">
          <img src="<?php echo get_template_directory_uri(); ?>/library/images/svg/header_jafanda-logo.svg" class="header__greenBoxImage">
      </div>
      <div class="header__largeBrand">EVERY BREATH COUNTS</div>
    </header>
